package algorismes;

import java.util.Scanner;

public class algorisme_13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		int A, B, C;
		double X;
		double R;
		
		System.out.println( "La equaci� de segon grau �s : (A^2)x + Bx + C");
		System.out.println("Digues un n�mero A");
		A = sc.nextInt();
		System.out.println("Digues un n�mero B");
		B = sc.nextInt();
		System.out.println("Digues un n�mero C");
		C = sc.nextInt();
		int raizcont = ((B*B) - 4 * A * C);

		if ( A == 0 )
			System.out.println( "A no pot ser 0" );
		else {
			
		if ( raizcont < 0 )
			System.out.println( "La equaci� no t� soluci�" );
		if ( raizcont == 0 ) {
			System.out.print( "La equaci� nom�s t� una arrel. " );
			R = ( - B ) / ( 2 * A );
			System.out.println( "La soluci� �s:" + " " + R);
			}
				
		if ( raizcont > 0 ) {
			System.out.println( "La equaci� t� dos solucions");
			R = ( - B ) + Math.sqrt(raizcont) / ( 2 * A );
			X = ( - B ) - Math.sqrt(raizcont) / ( 2 * A );
			System.out.println( "La soluci� �s: R = " + R + "i X = " + X);
			}
		}
		sc.close();
		
	}

}
