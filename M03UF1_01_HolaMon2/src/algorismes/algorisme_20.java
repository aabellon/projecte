package algorismes;

import java.util.Scanner;

public class algorisme_20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int dia, mes, any;
		int diaS, mesS, anyS;
		// Create new scanner
		Scanner sc = new Scanner(System.in);

		// Data d'entrada
		System.out.println("Data");
		System.out.println("- Digues un dia: ");
		dia = sc.nextInt();

		if (dia > 31 || dia <= 0) {
			System.out.println("- El dia es incorrecte");
			return;
		}

		System.out.println("- Digues un mes: ");
		mes = sc.nextInt();

		// Comprova els mesos
		if (mes > 12 || mes <= 0) {
			System.out.println("- El dia es incorrecte");
			return;
		}

		// Comprova el mes de febrer
		if (mes == 2 && dia > 28) {
			System.out.println("- El mes de febrer nom�s t� 28 dies");
			return;
		}

		System.out.println("- Digues un any: ");
		any = sc.nextInt();

		diaS = dia + 1;
		mesS = mes;
		anyS = any;

		switch (mes) {
		case 2:
			if (dia == 28) {
				diaS = 1;
				mesS++;
			}
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (dia == 30) {
				diaS = 1;
				mesS ++;
			}
			break;
			
		default:
			if (dia == 31) {
				diaS = 1;
				if(mes==12) {
					mesS = 1;
					anyS ++;
				}
				else
					mesS++;
			}
		}
		
		System.out.println("Avui �s " + dia + "/" + mes + "/" + any + " ... dem� ser�: " + diaS + "/" + mesS + "/" + anyS);
		sc.close();
	}

}
